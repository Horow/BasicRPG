﻿using BasicRPG.Graphics;
using BasicRPG.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace BasicRPG.Screens
{
    public class GameScreen : Screen
    {
        public GameScreen()
            : base()
        {

        }

        public override void Create()
        {
        }

        public override void Update(GameTime time)
        {
            UiManager.Update(time.ElapsedGameTime.Milliseconds);
            TimerManager.Update(time.ElapsedGameTime.Milliseconds);
        }

        public override void Draw()
        {
            spriteBatch.Begin();
            UiManager.Draw(spriteBatch);
            spriteBatch.End();
        }
    }
}